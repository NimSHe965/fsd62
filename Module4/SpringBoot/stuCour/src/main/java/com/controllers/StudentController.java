package com.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.StudentDao;
import com.model.Student;

@RestController
public class StudentController {
	
	@Autowired
	StudentDao stuDao;
	
	@GetMapping("stuLogin/{emailId}/{password}")
	public Student stuLogin(@PathVariable("emailId") String emailId, 
			@PathVariable("password") String password) {
		
		return stuDao.stuLogin(emailId, password);
	}
	
	@GetMapping("getAllStudents")
	public List<Student> getAllStudents() {
		return stuDao.getAllStudents();
	}
	
	@GetMapping("getStudentById/{id}")
	public Student getStudentById(@PathVariable("id") int stuId) {
		return stuDao.getStudentById(stuId);
	}
	
	@GetMapping("getStudentByName/{name}")
	public List<Student> getStudentByName(@PathVariable("name") String stuName) {
		return stuDao.getStudentByName(stuName);
	}
	
	@PostMapping("addStudent")
	public Student addStudent(@RequestBody Student stu) {
		return stuDao.addStudent(stu);
	}
	
	@PutMapping("updateStudent")
	public Student updateStudent(@RequestBody Student stu) {
		return stuDao.updateStudent(stu);
	}
	
	@DeleteMapping("deleteStudentById/{id}")
	public String deleteStudentById(@PathVariable("id") int stuId) {
		stuDao.deleteStudentById(stuId);
		return "Student Record Deleted Successfully!!!";
	}
}

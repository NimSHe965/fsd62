package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Student;

@Service
public class StudentDao {

	@Autowired
	StudentRepository stuRepo;
	
	public Student stuLogin(String emailId, String password) {
		return stuRepo.stuLogin(emailId, password);
	}

	public List<Student> getAllStudents() {
		return stuRepo.findAll();
	}

	public Student getStudentById(int stuId) {
		return stuRepo.findById(stuId).orElse(null);
	}

	public List<Student> getStudentByName(String stuName) {
		return stuRepo.findByName(stuName);
	}

	public Student addStudent(Student stu) {
		return stuRepo.save(stu);
	}
	
	public Student updateStudent(Student stu) {
		return stuRepo.save(stu);
	}

	public void deleteStudentById(int stuId) {
		stuRepo.deleteById(stuId);
	}
	
}

package com.dao.impl;

import com.dao.ProductDao;
import com.model.Product;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public abstract class ProductDaoImpl implements ProductDao {
    private List<Product> products = new ArrayList<>();

    @Override
    public List<Product> getAllProducts() {
        return products;
    }

    @Override
    public Product getProductById(Long id) {
        return products.stream().filter(product -> product.getId().equals(id)).findFirst().orElse(null);
    }

    @Override
    public Product addProduct(MultipartFile image, String name, String description, Double price) {
        // Implement logic to save the product and image to the database
        Product product = new Product();
        product.setName(name);
        product.setDescription(description);
        product.setPrice(price);
        // Save the product and image
        products.add(product);
        return product;
    }

    @Override
    public Product updateProduct(Long id, MultipartFile image, String name, String description, Double price) {
        // Implement logic to update the product and image in the database
        Product existingProduct = getProductById(id);
        if (existingProduct!= null) {
            existingProduct.setName(name);
            existingProduct.setDescription(description);
            existingProduct.setPrice(price);
            // Update the product and image
            return existingProduct;
        }
        return null;
    }

    @Override
    public void deleteProductById(Long id) {
    	
    	products.removeIf(product -> product.getId().equals(id));
    }
}

import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent implements OnInit {

  products: any;
  emailId: any;
  cartProducts: any;

  constructor(private service: EmpService) {

    //Making the CartProducts as an Array of Products
    this.cartProducts = [];
    this.emailId = localStorage.getItem('emailId'); 
  }
  ngOnInit(): void {
    this.service.getAllProducts().subscribe((data:any)=>{
      console.log(data);
      this.products=data;
    })
  }

  addToCart(product: any) {
    this.cartProducts.push(product);

    localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));
  }

}
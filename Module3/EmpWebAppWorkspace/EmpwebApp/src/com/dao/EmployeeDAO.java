package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.GetConnection;
import com.model.Employee;

public class EmployeeDAO {
	public Employee getEmployee(String emailID,String password){
		
		
		Connection con = GetConnection.getConnection();
		ResultSet rs = null;
		String query = "select * from empdetails where emailID =? and password =?";
		try{
			PreparedStatement pst = con.prepareStatement(query);
			pst.setString(1, emailID);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			Employee emp = new Employee();
			if(rs.next()){
				emp.setId(rs.getInt(1));
				emp.setName(rs.getString(2));
				emp.setemailId(rs.getString(3));
				emp.setpassword(rs.getString(4));
				emp.setsalary(rs.getDouble(5));
				
				return emp;
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
}

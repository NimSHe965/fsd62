package com.model;


public class Employee {
	 private int id;
	 private String empName;
	 private String emailId;
	 private String password;
	 private double salary;
	 public Employee(){
		 super();
		 
	 }
	 public Employee(String name,String emailId,String password,double salary, String empname, int id){
		super();
		this.id=id;
		 this.empName=empname;
		 this.emailId=emailId;
		 this.password=password;
		 this.salary=salary;
		 
	 }
	 public int getId(){
		 return id;
	 }
	 public void setId(int id){
		 this.id=id;
	 }
	 public String getName(){
		 return empName;
	 }
	 public void setName(String name){
		 this.empName=name;
	 }
	 public String getemailId(){
		 return emailId;
	 }
	 public void setemailId(String emailId){
		 this.emailId=emailId;
	 }
	 public String getpassword(){
		 return password;
	 }
	 public void setpassword(String password){
		 this.password=password;
	 }
	 public double getsalary(){
		 return salary;
	 }
	 public void setsalary(double salary){
		 this.salary=salary;
	 }
	 public String toString(){
		 return "Employee [id=" +id +", empName=" + empName +", salary=" + salary + ", emailId=" +emailId +", password=" + password +"]";
	 }
}

package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDAO;
import com.model.Employee;


@WebServlet("/Login")
public class Login extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String name = request.getParameter("userId");//HR userDetails
		String password =request.getParameter("password");//HR userPassword
		PrintWriter out = response.getWriter();
		RequestDispatcher rd = null;
		
		EmployeeDAO employeedao  =new EmployeeDAO();
	   Employee emp =	employeedao.getEmployee(name, password);//emp //null
		
		out.print("<html><body>");
		if(name.equals("HR")&&password.equals("HR")){		
			rd=request.getRequestDispatcher("HrHomePage");
			rd.forward(request, response);
		}else if(emp!=null){
			rd=request.getRequestDispatcher("EmployeeHomePage");
			rd.forward(request, response);
		}
		
		else{
			out.print("<h1>Invalid credentials...!!!");
			rd=request.getRequestDispatcher("Login.html");
			rd.include(request, response);
		}
		
		out.print("</body></html>");
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
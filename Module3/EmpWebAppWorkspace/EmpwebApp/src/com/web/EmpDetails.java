package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/RegisterServlet")
public class EmpDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
	        PrintWriter out = response.getWriter();

	        String employeeId = request.getParameter("employeeId");
	        String employeeName = request.getParameter("employeeName");
	        String password = request.getParameter("password");
	        String salary = request.getParameter("salary");
	        String gender = request.getParameter("gender");

	        out.println("<html><body>");
	        out.println("<h2>Employee Registration Details</h2>");
	        out.println("<p>Employee ID: " + employeeId + "</p>");
	        out.println("<p>Employee Name: " + employeeName + "</p>");
	        out.println("<p>Password: " + password + "</p>");
	        out.println("<p>Salary: " + salary + "</p>");
	        out.println("<p>Gender: " + gender + "</p>");
	        out.println("</body></html>");

	        out.close();
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
package com.model;


public class Student {
	
	private int StudentId;
	private String studentName;
	private String degree;
	private String gender;
	private String emailId;
	private String password;
	
	public Student() {
	}
	
	public Student(int StudentId, String studentName, String degree, String gender, String emailId, String password) {
		this.StudentId = StudentId;
		this.studentName = studentName;
		this.degree = degree;
		this.gender = gender;
		this.emailId = emailId;
		this.password = password;
	}

	public int getStudentId() {
		return StudentId;
	}

	public void setStudentId(int studentId) {
		StudentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Student [StudentId=" + StudentId + ", studentName=" + studentName + ", degree=" + degree + ", gender=" + gender
				+ ", emailId=" + emailId + ", password=" + password + "]";
	}

	
}


